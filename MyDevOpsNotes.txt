----------------------------------------------------------------------------------------------------
Install Prometheus Stack in Kubernetes
----------------------------------------------------------------------------------------------------

Deploy Monitoring Stack 
3 different ways to deploy the Prometheus monitoring stack 

1) Do it yourself 
- Create all configuration YAML files yourself 
- Execute them in right ord 
2) Using an Operator 
Manager of all Prometheus components 
- 1.Find Prometheus operator 
- 2.Deploy in K8s cluster 
3) Using Helm 
- Using Helm chart to deploy operator 
- Helm: Manage initial setup 
- Operator: Manage setup 

Overview of K8s resources deployed: 

3 Deployments 
- Prometheus Operator 
		- created Prometheus and Alertmanager StatefulSet 
- Grafana Kube 
- State Metrics 
		- => own Helm chart 
		- => dependency of this Helm chart 
		- => scrapes K8s components 
1 DaemonSet 
- Node Exporter DaemonSet 
		- => connects to server 
		- => translates Worker Nodes metrics to Prometheus metrics 
CPU usage 
load on server 

0. Useful Links: 
	- ●  Microservices App Git Repo: https://github.com/nanuchi/microservices-demo 
	- ●  Configuration Files for Microservices App: 
	  https: //gitlab.com/nanuchi/online-shop-microservices-deployment 
	- ●  Prometheus Helm Chart: 
	  https: //github.com/prometheus-community/helm-charts 
