# 16 - Monitoring with Prometheus - Install Prometheus Stack in Kubernetes

**Demo Project:**
Install Prometheus Stack in Kubernetes

**Technologies used:**
Prometheus, Kubernetes, Helm, AWS EKS, eksctl, Grafana, Linux

**Project Description:**
- Setup EKS cluster using eksctl
- Deploy Prometheus, Alert Manager and Grafana in cluster as part of the Prometheus Operator using Helm chart



16 - Monitoring with Prometheus
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





